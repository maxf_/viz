//! Formatting and output.

use crate::Color;
use crate::Day;
use crate::Log;

use std::io;
use std::io::Write;
use std::iter::{self, FromIterator};

use chrono::Timelike;

/// The length (in characters) of a color block for an activity.
const ACTIVITY_COLOR_BLOCK: usize = 3;

/// Marker for current time.
const CURRENT_TIME: char = '┃';
/// Marker for hours marked as "planned".
const PLANNED: char = '━';
/// Marker for current time if the hour is marked as "planned".
const CURRENT_TIME_AND_PLANNED: char = '╋';

/// An output writer.
///
/// This trait is used to define an output modes.
pub trait Writer: Sized {
    /// The inner IO writer.
    type Inner: io::Write;

    /// Get the inner IO writer.
    fn io_write(&mut self) -> &mut Self::Inner;
    /// Print a `color`-colored block of length `length` with character `ch`.
    fn print_color(&mut self, color: Color, length: usize, ch: char) -> io::Result<()>;
    // TODO: Pass a reference instead, e.g. use iterators and higher order type, or anonymous
    //       implementations.
    /// Iterate over `days` applying `f` to each item and finalize with `summary`.
    ///
    /// This is used to control the ordering of the entries (which could for example be reversed).
    ///
    /// The default implementation simply runs in the order of `days` and then `summary`. If any
    /// of these calls fails, the error is returned.
    fn for_each_day<'a, F, G>(
        mut self,
        days: &'a [Day<'a>],
        mut f: F,
        mut summary: G,
    ) -> io::Result<()>
    where
        F: FnMut(&mut Self, &'a Day<'a>) -> io::Result<()>,
        G: FnMut(&mut Self) -> io::Result<()>,
    {
        for day in days {
            f(&mut self, &day)?;
        }

        // Afterwards, write the summary.
        summary(&mut self)
    }
}

/// Terminal-based frontend.
pub struct Terminal<W>(pub W);

impl<W: io::Write> Writer for Terminal<W> {
    type Inner = W;

    fn io_write(&mut self) -> &mut W {
        &mut self.0
    }

    fn print_color(&mut self, color: Color, length: usize, ch: char) -> io::Result<()> {
        // Set the color.
        match color {
            Color::Black => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::Black)
            )?,
            Color::White => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::White)
            )?,
            Color::Red => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::Red)
            )?,
            Color::Blue => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::Blue)
            )?,
            Color::Green => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::Green)
            )?,
            Color::Yellow => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::Yellow)
            )?,
            Color::Cyan => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::Cyan)
            )?,
            Color::LightRed => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::LightRed)
            )?,
            Color::LightBlue => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::LightBlue)
            )?,
            Color::LightGreen => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::LightGreen)
            )?,
            Color::LightYellow => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::LightYellow)
            )?,
            Color::LightCyan => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::LightCyan)
            )?,
            Color::Rgb(x, y, z) => write!(
                self.io_write(),
                "{}",
                termion::color::Bg(termion::color::Rgb(x, y, z))
            )?,
            // Instead of using escape codes, use the Unicode block characters.
            Color::Grey1 => {
                for _ in 0..length {
                    write!(self.io_write(), "█")?;
                }
                return Ok(());
            }
            Color::Grey2 => {
                for _ in 0..length {
                    write!(self.io_write(), "▓")?;
                }
                return Ok(());
            }
            Color::Grey3 => {
                for _ in 0..length {
                    write!(self.io_write(), "▒")?;
                }
                return Ok(());
            }
        }

        // Write the blocks.
        write!(self.io_write(), "{}", termion::color::Fg(termion::color::Black))?;
        for _ in 0..length {
            write!(self.io_write(), "{}", ch)?;
        }
        // Reset the color.
        write!(
            self.io_write(),
            "{}{}",
            termion::color::Bg(termion::color::Reset),
            termion::color::Fg(termion::color::Reset),
        )
    }
}

/// Rofi (using Pango markup) frontend.
pub struct Rofi<W>(pub W);

impl<W: io::Write> Writer for Rofi<W> {
    type Inner = W;

    fn io_write(&mut self) -> &mut W {
        &mut self.0
    }

    fn print_color(&mut self, color: Color, length: usize, ch: char) -> io::Result<()> {
        // Start the tag.
        write!(self.io_write(), "<span bgcolor=\"")?;

        // Write the color.
        match color {
            Color::Black => write!(self.io_write(), "#000000")?,
            Color::White => write!(self.io_write(), "#FFFFFF")?,
            Color::Red => write!(self.io_write(), "#FF0000")?,
            Color::Blue => write!(self.io_write(), "#18AEFe")?,
            Color::Green => write!(self.io_write(), "#79C307")?,
            Color::Yellow => write!(self.io_write(), "#F96808")?,
            Color::Cyan => write!(self.io_write(), "#67D9F0")?,
            Color::LightRed => write!(self.io_write(), "#F12872")?,
            Color::LightBlue => write!(self.io_write(), "#37C2FF")?,
            Color::LightGreen => write!(self.io_write(), "#90FF0A")?,
            Color::LightYellow => write!(self.io_write(), "#F9DC27")?,
            Color::LightCyan => write!(self.io_write(), "#1BC4EF")?,
            Color::Rgb(x, y, z) => write!(self.io_write(), "#{:02X}{:02X}{:02X}", x, y, z)?,
            Color::Grey1 => write!(self.io_write(), "#DCDCDC")?,
            Color::Grey2 => write!(self.io_write(), "#A9A9A9")?,
            Color::Grey3 => write!(self.io_write(), "#696969")?,
        }

        write!(self.io_write(), "\">")?;

        // Write the blocks.
        write!(self.io_write(), "<span fgcolor=\"black\">")?;
        for _ in 0..length {
            write!(self.io_write(), "{}", ch)?;
        }

        // End the tags.
        write!(self.io_write(), "</span></span>")
    }

    fn for_each_day<'a, F, G>(
        mut self,
        days: &'a [Day<'a>],
        mut f: F,
        mut summary: G,
    ) -> io::Result<()>
    where
        F: FnMut(&mut Self, &'a Day<'a>) -> io::Result<()>,
        G: FnMut(&mut Self) -> io::Result<()>,
    {
        // Write the summary first.
        summary(&mut self)?;

        // Reverse the order to have the newest first.
        for day in days.iter().rev() {
            f(&mut self, day)?;
        }
        Ok(())
    }
}

impl<'a> Log<'a> {
    pub fn print<W: Writer>(&self, writer: W, parameter: Option<&'a str>) -> io::Result<()> {
        // Get current time.
        let now = chrono::Local::now();

        writer.for_each_day(
            &self.days,
            |writer, day| {
                // This will contain the types of the respective hours in the day.
                let mut types = Vec::new();

                // Write the date.
                write!(writer.io_write(), "{}: ", day.date)?;

                // Go over the activities.
                for (hr_num, hr) in day.hours.iter().enumerate() {
                    let activity = &self.activities[hr.activity];
                    // Write the color associated to the activity.
                    if hr_num == now.hour() as usize {
                        // The hour is the current hour.

                        // Figure out where the vertical marker telling the time should be placed.
                        let pointer_position = now.minute() as usize * ACTIVITY_COLOR_BLOCK / 60;
                        // Go over each character of the block and determine the right marker.
                        for i in 0..ACTIVITY_COLOR_BLOCK {
                            if i == pointer_position {
                                if hr.planned {
                                    // Print an intersection of the marker for planned events and
                                    // current hour.
                                    writer.print_color(
                                        activity.color,
                                        1,
                                        CURRENT_TIME_AND_PLANNED,
                                    )?;
                                } else {
                                    // Print the vertical marker pointing to the time.
                                    writer.print_color(
                                        activity.color,
                                        1,
                                        CURRENT_TIME,
                                    )?;
                                }
                            } else {
                                // This part of the hour is not the current.

                                // Print the appropriate label.
                                if hr.planned {
                                    writer.print_color(
                                        activity.color,
                                        1,
                                        PLANNED,
                                    )?;
                                } else {
                                    writer.print_color(
                                        activity.color,
                                        1,
                                        ' ',
                                    )?;
                                }
                            }
                        }
                    } else if hr.planned {
                        // The hour was planned.
                        writer.print_color(
                            activity.color,
                            ACTIVITY_COLOR_BLOCK,
                            PLANNED,
                        )?;
                    } else {
                        // The hour was completed.
                        writer.print_color(
                            activity.color,
                            ACTIVITY_COLOR_BLOCK,
                            ' ',
                        )?;
                    };
                    // Add the type.
                    types.push(activity.type_color);
                }

                // Pad with spaces to ensure alignment in case of incomplete rows.
                for _ in day.hours.len() * ACTIVITY_COLOR_BLOCK..24 * ACTIVITY_COLOR_BLOCK + 1 {
                    write!(writer.io_write(), " ")?;
                }

                // Write number of productive hours.
                write!(writer.io_write(), "{:02}:{:02} ",
                    day.productive_minutes / 60,
                    day.productive_minutes % 60)?;

                // Sort the types to nicely show the ratio.
                types.sort_by_key(|x| x.greyness());
                // Print the ratio of the types.
                for color in types {
                    writer.print_color(color, 1, ' ')?;
                }

                write!(writer.io_write(), " ")?;

                // Pad with spaces to ensure alignment in case of incomplete rows.
                for _ in day.hours.len()..24 {
                    write!(writer.io_write(), " ")?;
                }

                // Write the letter matrix.
                if !self.letters.is_empty() {
                    write!(writer.io_write(), "[")?;
                }
                for ltr in &self.letters {
                    if day.letters.contains(ltr) {
                        write!(writer.io_write(), "{}", ltr)?;
                    } else {
                        write!(writer.io_write(), " ")?;
                    }
                }
                if !self.letters.is_empty() {
                    write!(writer.io_write(), "]")?;
                }

                // Write the parameter column.
                if let Some(parameter) = parameter {
                    write!(writer.io_write(), " {}", day.parameters.get(parameter).unwrap_or(&""))?;
                }

                // End of line.
                write!(writer.io_write(), "\n")
            },
            |writer| {
                // Print the hour numbers and potential parameter name.
                if let Some(parameter) = parameter {
                    // If the parameter is not recognized, we panick. This will not be happen, as the
                    // error is handled in the main routine.
                    let parameter_data = &self.parameters[parameter];

                    write!(writer.io_write(), "             00 01 02 03 04 05 06 07 08 09 10 11 \
                        12 13 14 15 16 17 18 19 20 21 22 23{}{}{}",
                        String::from_iter(iter::repeat(' ').take(
                            34 +
                            self.letters.len() +
                            // Take into account the `[` and `]`.
                            if self.letters.is_empty() { 0 } else { 2 }
                        )),
                        parameter,
                        if parameter_data.description.is_empty() {
                            String::new()
                        } else {
                            format!(" ({})", parameter_data.description)
                        })?;
                } else {
                    write!(writer.io_write(), "             00 01 02 03 04 05 06 07 08 09 10 11 \
                        12 13 14 15 16 17 18 19 20 21 22 23")?;
                }

                // End of line.
                write!(writer.io_write(), "\n")
            },
        )
    }
}
